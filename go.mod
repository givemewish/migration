module gitlab.com/givemewish/migration

go 1.16

require (
	github.com/golang-migrate/migrate/v4 v4.14.1
	gitlab.com/givemewish/logger v1.0.2
)
