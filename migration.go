package migration

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"path"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/golang-migrate/migrate/v4/source"
	"gitlab.com/givemewish/logger"
)

func EnsureDB(ctx context.Context, db *sql.DB, assets http.FileSystem) error {
	if assets == nil {
		return nil
	}

	dbdrv, err := postgres.WithInstance(db, &postgres.Config{})
	if err != nil {
		return fmt.Errorf("create driver with instance: %w", err)
	}

	s, err := Source{
		Assets: assets,
	}.Open("")
	if err != nil {
		return fmt.Errorf("open source: %w", err)
	}

	migrator, err := migrate.NewWithInstance("vfs", s, "postgres", dbdrv)
	if err != nil {
		return fmt.Errorf("create migrator with instance: %w", err)
	}

	err = migrator.Up()
	if err != nil && err != migrate.ErrNoChange {
		return fmt.Errorf("apply migration: %w", err)
	}

	if err == migrate.ErrNoChange {
		logger.GetLogger(ctx).WithField("actor", "migration").Info("no changes")
	}

	return nil
}

type Source struct {
	Assets     http.FileSystem
	opened     http.File
	migrations *source.Migrations
}

func (s Source) Close() error {
	return s.opened.Close()
}

var ErrAppend = errors.New("append to migrations")

func (s Source) Open(url string) (source.Driver, error) {
	ns := s
	var err error
	ns.opened, err = s.Assets.Open("/")
	if err != nil {
		return nil, err
	}
	files, err := ns.opened.Readdir(-1)
	if err != nil {
		return nil, err
	}
	ns.migrations = source.NewMigrations()
	for _, f := range files {
		if !f.IsDir() {
			m, err := source.DefaultParse(f.Name())
			if err != nil {
				continue
			}
			if !ns.migrations.Append(m) {
				return nil, ErrAppend
			}
		}
	}
	return ns, nil
}

func (s Source) First() (version uint, err error) {
	if v, ok := s.migrations.First(); ok {
		return v, nil
	}
	return 0, &os.PathError{Op: "first", Path: "/", Err: os.ErrNotExist}
}

func (s Source) Prev(version uint) (prevVersion uint, err error) {
	if v, ok := s.migrations.Prev(version); ok {
		return v, nil
	}

	return 0, &os.PathError{Op: fmt.Sprintf("prev for version %v", version), Path: "/", Err: os.ErrNotExist}
}

func (s Source) Next(version uint) (nextVersion uint, err error) {
	if v, ok := s.migrations.Next(version); ok {
		return v, nil
	}

	return 0, &os.PathError{Op: fmt.Sprintf("next for version %v", version), Path: "/", Err: os.ErrNotExist}
}

func (s Source) ReadUp(version uint) (r io.ReadCloser, identifier string, err error) {
	if m, ok := s.migrations.Up(version); ok {
		r, err := s.Assets.Open(path.Join("/", m.Raw))
		if err != nil {
			return nil, "", err
		}
		return r, m.Identifier, nil
	}
	return nil, "", &os.PathError{Op: fmt.Sprintf("read version %v", version), Path: "/", Err: os.ErrNotExist}
}

func (s Source) ReadDown(version uint) (r io.ReadCloser, identifier string, err error) {
	if m, ok := s.migrations.Down(version); ok {
		r, err := s.Assets.Open(path.Join("/", m.Raw))
		if err != nil {
			return nil, "", err
		}
		return r, m.Identifier, nil
	}
	return nil, "", &os.PathError{Op: fmt.Sprintf("read version %v", version), Path: "/", Err: os.ErrNotExist}
}
